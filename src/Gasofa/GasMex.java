/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gasofa;

/**
 *
 * @author luisr
 */
public class GasMex {
    
    private int codigoVenta;
    private int cantidad;
    private int tipo;
    private float precio;

    public GasMex() {
        this.cantidad = 0;
        this.codigoVenta = 0;
        this.precio = 0.0f;
        this.tipo = 0;
    }

    public GasMex(int codigoVenta, int cantidad, int tipo, float precio) {
        this.codigoVenta = codigoVenta;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.precio = precio;
    }
     
    public GasMex(GasMex x){
    this.cantidad = x.cantidad ;
    this.codigoVenta = x.codigoVenta;
    this.precio = x.precio;
    this.tipo = x.tipo;
 }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float CalcularCosto(){
    
    float total = 0.0f;
    total= this.cantidad * this.tipo;
    return total;
    }
    
}
